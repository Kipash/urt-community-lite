# Urban Terror server discord widget


First read out the documentation that contains simple guides and more information.
# [Documentation](https://gitlab.com/Kipash/urt-community-lite/-/wikis/home)

## Known issues ❗

### Bots are not visible while using rcon
The official binary and the m9 binary both report bots incorrectly. It is mostly a bug and the bots never report their team the same way how it is reported about players. There is no other way how to obtain that information. __Only solution is to use community-maintained binaries which do not have this issue__. In other words, the server has to use a different binary. Be cautious, I can't guarantee the safety of community maintained code. But to be productive, a functioning example is the [urt slim server](https://github.com/omg-urt/urbanterror-slim/releases/tag/latest). 

Here is a full explanation of what exactly is going on. As you can see, the last 5 lines are the information about a specific user. A player on a slot 0 with a nick and K D A is nicely seen. But a bot has only the [connecting] there. This happens to players as well, but only while they are connecting. The bots are reported like this for the whole game so there is no way how to obtain their team and K D A.

`/rcon players`
```
Map: ut4_abbey
Players: 13
GameType: CTF
Scores: R:0 B:0
MatchMode: OFF
WarmupPhase: NO
GameTime: 00:14:01
0:Kipash TEAM:RED KILLS:O DEATHS:O ASSISTS:0 PING:0 AUTH:--- IP:localhost
[connecting] Bot_1
[connecting] Bot_2
[connecting] Bot_3
[connecting] Bot_4
```

---

### Download my build

Download the updater from my page: <https://chrisstman.eu/UCT/> and run it on the server.

> `wget https://chrisstman.eu/UCT/repo/archive/1.0.5/UrTPanel_Setup_v1.0.5.sh`

> `sh UrTPanel_Setup_v1.0.5.sh`

---

### Compiling it yourself (optional)

The bot uses `.NET 7`

Then simply cd into the solution and compile it. Notice the `PublishSingleFile` will pack all dependencies together which is simpler than installing the .NET core runtime on targeted machines. Might not suit everyone, so there's still an option to compile it without the flag and it should just create the slim .dll which can be executed like this `dotnet UrTPanel.dll`

Linux:
> `dotnet publish -r linux-x64 -c Release /p:PublishSingleFile=true`

Windows:
> `dotnet publish -r win-x64 -c Release /p:PublishSingleFile=true`

---

### Running it

Running the bot is fairly simple.

To start the bot: `./UrTPanel_Start.sh`

To stop the bot: `./UrTPanel_Stop.sh`
 
---

### Encountered an error?

1 - contact the creator on discord (the discord server is provided on [the website](https://chrisstman.eu/UCT/)).

2 - if you are the owner of the bot, you can DM the bot and diagnose what problems there are. `!help` will reveal the debug commands. Optionally ask the developer (kipash) and he can debug the bot as well. No sensitive information is ever revieled. (tokens, rcon passwords)

3 - running the loop itself will reveal much more information. (kill the bot first and then run $ `./UCT_ServerPanel_Loop.sh`). If the error doesn't make sense to you, continue with step #1.

---

### Special thanks

__FuSo__ for ideating the concept of this bot.

Different communities that expressed their needs and helped to improve the bot
 - UrT España (Raffa)
 - Urban Gamers (Asger)
 - Drunken Noobs 
 - Wasted Goats (Fesk, Blaze)
