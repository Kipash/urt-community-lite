﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace UrtPanel
{
    /// <summary>
    /// Utility function to connect and fetch data from a Urban Terror game server
    ///     - Connect
    ///     - Send command & read back response
    /// </summary>
    public class UrTServerUtil
    {
        public static Socket Connect(string serverIP, int port)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.ReceiveTimeout = 3000;
            socket.SendTimeout = 3000;

            try
            {
                //attempts to connect
                socket.Connect(serverIP, port);
                Log.Write($"Connected to server {serverIP} on port:{port}", LogCol.DarkGreen);
            }
            catch (Exception e)
            {
                //connect failed
                Log.Write($"<Error> Can't connect to server {serverIP} on port:{port}\n\nMore details: {e}", LogCol.Red);
                socket.Dispose();
                return null;
            }

            return socket;
        }
        static byte[] GenerateByteArray(string toAppend)
        {
            byte[] bufferTemp = Encoding.ASCII.GetBytes(toAppend);
            byte[] bufferSend = new byte[bufferTemp.Length + 5];

            bufferSend[0] = byte.Parse("255");
            bufferSend[1] = byte.Parse("255");
            bufferSend[2] = byte.Parse("255");
            bufferSend[3] = byte.Parse("255");
            int j = 4;

            for (int i = 0; i < bufferTemp.Length; i++)
            {
                bufferSend[j++] = bufferTemp[i];
            }

            return bufferSend;
        }

        static byte[] Send(Socket socket, string payload)
        {
            //generates payload
            byte[] toSend = GenerateByteArray(payload);

            //send rcon command and get response
            //IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            socket.Send(toSend);

            //big enough to receive response
            byte[] bufferRec = new byte[1024 * 1024];

            var start = DateTime.Now;
            int received = 0;
            int hardwait = 500; //ms
            while (true)
            {
                var read = socket.Receive(bufferRec, received, bufferRec.Length - received, SocketFlags.None);
                received += read;

                while (DateTime.Now.Subtract(start).TotalMilliseconds < hardwait)
                {
                    Thread.Sleep(10);
                }

                if (socket.Available == 0)
                    break;
                else
                    continue;
            }

            var buf = bufferRec.Where(x => x != 0).ToArray();
            return buf;
        }
        public static bool FetchServerInfo(Socket socket, out string response, out string error)
        {
            return FetchCommand(socket, "getstatus", out response, out error);
        }

        public static bool FetchPlayerInfo(Socket socket, string rconPass, out string response, out string error)
        {
            return FetchCommand(socket, $"rcon {rconPass} players", out response, out error);
        }

        public static bool FetchCommand(Socket socket, string command, out string response, out string error)
        {
            error = null;
            try
            {
                var dataRaw = Send(socket, command);
                response = Encoding.ASCII.GetString(dataRaw);
                return true;
            }
            catch (Exception e)
            {
                var ipep = socket.RemoteEndPoint as IPEndPoint;
                string ip = ipep != null ? $"{ipep.Address}:{ipep.Port}" : "";
                error = $"({ip}) Server error:\n```{e.Message}\n{e.StackTrace}```\n";
                Log.Write($"{e.Message}\n{e.StackTrace}", LogCol.Red);
                response = string.Empty;
                return false;
            }
        }

        /*
        public static bool GetServerInfo(Socket socket, out string response, out string error)
        {
            error = null;
            try
            {
                var dataRaw = Send(socket, "getstatus");
                response = Encoding.ASCII.GetString(dataRaw);
                return true;
            }
            catch(Exception e)
            {
                var ipep = socket.RemoteEndPoint as IPEndPoint;
                string ip = ipep != null ? $"{ipep.Address}:{ipep.Port}" : "";
                error = $"({ip}) Server error:\n```{e.Message}\n{e.StackTrace}```\n";
                Log.Write($"{e.Message}\n{e.StackTrace}", LogCol.Red);
                response = string.Empty;
                return false;
            }
        }

        public static bool GetServerInfoRcon(Socket socket, string rcon, out string response, out string error)
        {
            error = null;

            try
            {
                var dataRaw = Send(socket, $"rcon {rcon} players");
                response = Encoding.ASCII.GetString(dataRaw);
                return true;
                
            }
            catch (Exception e)
            {
                var ipep = socket.RemoteEndPoint as IPEndPoint;
                string ip = ipep != null ? $"{ipep.Address}:{ipep.Port}" : "";
                error = $"({ip}) Server error:\n```{e.Message}\n{e.StackTrace}```\n";
                Log.Write($"{e.Message}\n{e.StackTrace}", LogCol.Red);
                response = string.Empty;
                return false;
            }
        }*/
    }
}