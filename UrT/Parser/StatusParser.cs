﻿using System;
using System.Linq;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;

namespace UrtPanel.Parser
{
    public class StatusParser
    {
        public static void ParseCvars(string status, ServerInfo server)
        {
            server.Cvars.Clear(); //default

            var serverDataSegments = status.Split('\n');
            //server.Error_NoStatus = serverDataSegments.Length < 2;

            var settings = serverDataSegments[1];

            //settings
            int sectionNameStart = 0;
            int sectionNameEnd = 0;

            int valueStart = 0;
            int valueEnd = 0;

            int level = 0;
            for (int i = 0; i < settings.Length; i++)
            {
                char c = settings[i];
                if (c == '\\')
                {
                    level++;
                    if (level == 1)
                    {
                        sectionNameStart = i + 1;
                    }
                    if (level == 2)
                    {
                        sectionNameEnd = i - 1;
                        valueStart = i + 1;
                    }
                    if (level == 3)
                    {
                        valueEnd = i - 1;

                        var sectionName = settings.Substring(sectionNameStart, sectionNameEnd - sectionNameStart + 1);
                        var value = settings.Substring(valueStart, valueEnd - valueStart + 1);
                        server.Cvars.Add(sectionName.ToLower(), value);

                        level = 1;
                        sectionNameStart = i + 1;
                    }
                }
                if (i == settings.Length - 1)
                {
                    valueEnd = i;

                    var sectionName = settings.Substring(sectionNameStart, sectionNameEnd - sectionNameStart + 1);
                    var value = settings.Substring(valueStart, valueEnd - valueStart + 1);
                    server.Cvars.Add(sectionName.ToLower(), value);
                }
            }

            void TryAddBool(string cvarName)
            {
                if (server.Cvars.TryGetValue(cvarName, out string tempVal))
                {
                    string result = "";
                    if (tempVal == "1")
                        result = "Yes";
                    else
                        result = "No";

                    server.Cvars.Remove(cvarName);
                    server.Cvars.Add(cvarName, result);
                }
                else
                    Console.WriteLine($"{cvarName} not present");
            };


            TryAddBool("auth");
            TryAddBool("g_funstuff");
            TryAddBool("g_instagib");
            TryAddBool("g_matchmode");
            TryAddBool("g_skins");
            TryAddBool("g_suddendeath");
            TryAddBool("g_waverespawns");

            //not FFA, LSM, Jump
            server.Cvars.TryGetValue("g_gametype", out string gameTypeString);
            int.TryParse(gameTypeString, out int gameType);
            server.IsTeamMode = gameType != 0 && gameType != 1 && gameType != 9 && gameType != 11; //ugly
        }

        public static void ParsePlayers_Status(string status, ServerInfo server)
        {
            var serverDataSegments = status.Split('\n');

            server.StatusData = status;
            server.Players.Clear();

            if (serverDataSegments.Length > 3)
            {
                var playerLines = serverDataSegments.Skip(2).SkipLast(1);
                //serverInfo += $"*Total players: __{numOfPlayers}__ ({maxPlayers}):*\n";
                foreach (var x in playerLines)
                {
                    var player = new PlayerInfo();

                    var sections = x.Split(' ');
                    var score = sections[0];
                    var ping = sections[1];
                    var nick = string.Join(' ', sections.Skip(2).ToArray());
                    nick = nick.UrTSanetize();

                    player.Nick = nick;
                    player.Score = score;
                    player.IsBot = ping == "0";
                    player.Team = Team.none;

                    server.Players.Add(player);
                }
            }

            //remove bots based on settings
            if (!server.Settings.ShowBots)
                server.Players.RemoveAll(x => x.IsBot);

            if (server.Cvars.TryGetValue("sv_maxclients", out string max) && server.Cvars.TryGetValue("sv_privateclients", out string priv))
            {
                var currPlayers = serverDataSegments.Length - 3;
                server.Cvars.Add("players", $"{currPlayers} / {max} ({priv})");
            }
            else
                Console.WriteLine("Cvars not there");
        }

        public static void ParsePlayers_Rcon(string gameData, ServerInfo server, MainConfig cfg)
        {
            server.RconData = gameData;

            if (string.IsNullOrEmpty(gameData))
            {
                server.HasRconInfo = false;
                return;
            }
            if (gameData.Contains("Bad rcon"))
            {
                server.InvalidRconPassword = true;
                server.HasRconInfo = false;
                return;
            }
            else
                server.InvalidRconPassword = false;

            Log.Write(gameData, LogCol.Gray);

            var loc = cfg.DiscordConfig.Localization;
            server.HasRconInfo = true;

            var splits = gameData.Split('\n');

            splits = splits.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            splits = splits.Skip(2).ToArray(); //removing map name 

            int clients = 0;
            var clientsSection = splits.FirstOrDefault(x => x.Contains("Players:"));
            if (clientsSection != null)
            {
                int.TryParse(clientsSection.Substring(9), out clients);
            }

            //Parsed input:     Scores: R:1234 B:1234
            var score = splits.FirstOrDefault(x => x.Contains("Scores:"));
            if (score != default)
            {
                string redScore = "", blueScore = "";
                var sSplits = score.Split(' ');
                if (sSplits.Length >= 2) // TODO: replace with regex just for clarity (?)
                {
                    redScore = new string(sSplits[1].Skip(2).ToArray());
                    blueScore = new string(sSplits[2].Skip(2).ToArray());
                }
                server.Cvars.Add("redScore", redScore);
                server.Cvars.Add("blueScore", blueScore);
            }

            //Game time
            var gameTime = splits.FirstOrDefault(x => x.Contains("GameTime:"));
            if (!string.IsNullOrEmpty(gameTime))
                gameTime = new string(gameTime.Skip(10).ToArray());

            if (gameTime != default)
            {
                server.Cvars.Add("gametime", gameTime);
            }

            //5:John TEAM:BLUE KILLS:8 DEATHS:10 ASSISTS:1 PING:0 AUTH:--- IP:bot
            //All lines which start with a number
            var playerLines = splits.Where(x => int.TryParse(x[0].ToString(), out int i)).ToArray();
            foreach (var x in playerLines)
            {
                var playerSlotLen = x.Split(':')[0].Length + 1;
                var plData = new string(x.Skip(playerSlotLen).ToArray());

                Log.Write(plData, LogCol.DarkMagenta);

                Console.WriteLine();
                var plSplits = plData.Split(' ');
                Log.Write(string.Join("\n", plSplits), LogCol.DarkBlue);
                var plName = plSplits[0];
                var plTeam = plSplits[1].Skip(5);
                var plKills = plSplits[2].Skip(6);
                var plDeaths = plSplits[3].Skip(7);
                var plAssists = plSplits[4].Skip(8);
                var plPing = plSplits[5].Skip(5);
                var plAuth = plSplits[6].Skip(5);
                var plIP = plSplits[7].Skip(3);

                var nick = new string(plName.ToArray());
                var player = server.Players.SingleOrDefault(x => x.Nick.Replace(" ", "") == nick.Replace(" ", "")); //remove spaces, bug in official binary
                if (player == null)
                {
                    player = new PlayerInfo();
                    server.Players.Add(player);
                }

                player.Nick = nick;
                player.Kills = new string(plKills.ToArray());
                player.Deaths = new string(plDeaths.ToArray());
                player.Assists = new string(plAssists.ToArray());
                player.Auth = new string(plAuth.ToArray());
                player.IsBot = new string(plIP.ToArray()) == "bot";
                player.Score = "0";

                Enum.TryParse(new string(plTeam.ToArray()), true, out player.Team);
            }

            //[connecting] Bill
            var connectingPlayers = splits.Where(x => x[0] == '[').ToArray();
            foreach (var x in connectingPlayers)
            {
                var name = x.Split(' ')[1];
                server.Players.Add(new PlayerInfo()
                {
                    Nick = name,
                    Team = Team.none,
                    Auth = "---",
                });
            }

            //remove bots based on settings
            if (!server.Settings.ShowBots)
                server.Players.RemoveAll(x => x.IsBot);

            server.RedTeam = server.Players.Where(x => x.Team == Team.Red).ToList();
            server.BlueTeam = server.Players.Where(x => x.Team == Team.Blue).ToList();
            server.FreeTeam = server.Players.Where(x => x.Team == Team.Free).ToList();
            server.SpecTeam = server.Players.Where(x => x.Team == Team.Spectator).ToList();
        }
    }
}
