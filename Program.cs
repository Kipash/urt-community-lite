﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;
using UrtPanel.Config;
using UrtPanel.Panel;

namespace UrtPanel
{
    public class Program
    {
        public const string Version = "v1.0.5";

        const string IntroScreen = @$"
     __  __         ______      ____                         ___      
    /\ \/\ \       /\__  _\    /\  _`\                      /\_ \     
    \ \ \ \ \  _ __\/_/\ \/    \ \ \L\ \ __      ___      __\//\ \    
     \ \ \ \ \/\`'__\ \ \ \     \ \ ,__/'__`\  /' _ `\  /'__`\\ \ \   
      \ \ \_\ \ \ \/   \ \ \     \ \ \/\ \L\.\_/\ \/\ \/\  __/ \_\ \_ 
       \ \_____\ \_\    \ \_\     \ \_\ \__/.\_\ \_\ \_\ \____\/\____\
        \/_____/\/_/     \/_/      \/_/\/__/\/_/\/_/\/_/\/____/\/____/


               Made by Kipash for the Urban Terror community.

                                  {Version}


";

        DiscordSocketClient client;
        CommandService commands;
        IServiceProvider services;
        MainConfig config;
        static void Main(string[] args)
        {
            Log.Write(IntroScreen, LogCol.Cyan);

            new Program()
                .RunBotAsync()
                .GetAwaiter()
                .GetResult();
        }

        public async Task RunBotAsync()
        {
            client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                GatewayIntents = GatewayIntents.AllUnprivileged | GatewayIntents.MessageContent
            });

            commands = new CommandService();

            config = MainConfig.LoadConfig();

            var session = new ChannelSession(config);

            services = new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .AddSingleton(config)
                .AddSingleton(session)
                .BuildServiceProvider();

            var token = config.DiscordConfig.Token;

            client.Log += ClientLog;
            await RegisterCommandsAsync();
            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();

            // TODO: add multi session support
            await session.DisplayContentLoop(client);
            await Task.Delay(-1);
        }

        Task ClientLog(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            client.MessageReceived += HandleCommandAsync;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }

        async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            var context = new SocketCommandContext(client, message);
            if (message.Author.IsBot) return;

            var prefix = config.DiscordConfig.CommandPrefix;

            int argPos = 0;
            if (message.HasStringPrefix(prefix, ref argPos))
            {
                var result = await commands.ExecuteAsync(context, argPos, services);
                if (!result.IsSuccess) Console.WriteLine(result.ErrorReason);
            }
        }
    }


    public class Log
    {
        public static void Write(string msg, LogCol col)
        {
            var currCol = Console.ForegroundColor;
            Console.ForegroundColor = (ConsoleColor)(int)col;
            Console.WriteLine(msg);
            Console.ForegroundColor = currCol;
        }
    }

    public enum LogCol
    {
        Black = 0,
        DarkBlue = 1,
        DarkGreen = 2,
        DarkCyan = 3,
        DarkRed = 4,
        DarkMagenta = 5,
        DarkYellow = 6,
        Gray = 7,
        DarkGray = 8,
        Blue = 9,
        Green = 10,
        Cyan = 11,
        Red = 12,
        Magenta = 13,
        Yellow = 14,
        White = 15
    }
}
