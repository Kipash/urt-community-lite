﻿using Discord;
using System.Collections.Generic;
using System.Linq;

namespace UrtPanel.Panel
{
    public class ServerPanel
    {
        //NEW
        public List<EmbedData> MapPreview = new();
        public List<EmbedData> PlayerLists = new();
        public List<EmbedData> Other = new();

        public List<EmbedData> GetEmbeds() => Concat(MapPreview, PlayerLists, Other);

        List<T> Concat<T>(params List<T>[] lists) => lists.SelectMany(x => x).ToList();
    }

    public class EmbedData
    {
        public string Title;
        public string ImageUrl;
        public string FooterIconUrl;
        public string FooterText;

        public List<EmbedFiled> Fields = new();

        public void AddField(string title, string content, bool inline = false) => Fields.Add(new(title, content, inline));
        public void AddFieldTop(string title, string content, bool inline = false) => Fields.Insert(0, new(title, content, inline));

        public EmbedBuilder GetBuilder()
        {
            var builders = new EmbedBuilder();

            builders.Title = Title;
            builders.ImageUrl = ImageUrl;

            foreach (var x in Fields)
                builders.AddField(x.Title, x.Content, x.Inline);

            if (FooterIconUrl != null || FooterText != null)
            {
                builders.Footer = new EmbedFooterBuilder();
                builders.Footer.IconUrl = FooterIconUrl;
                builders.Footer.Text = FooterText;
            }

            return builders;
        }
    }

    public struct EmbedFiled
    {
        public string Title;
        public string Content;
        public bool Inline;

        public EmbedFiled(string key, string value, bool inline)
        {
            Title = key;
            Content = value;
            Inline = inline;
        }
    }
}
