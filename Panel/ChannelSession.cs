﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;
using UrtPanel.Parser;

namespace UrtPanel.Panel
{
    public class ChannelSession
    {
        public ReadOnlyCollection<string> ExecutionErrors;
        List<string> executionErrors = new();

        public ServerInfo[] Servers = new ServerInfo[0];

        MessageConstructor constructor;
        MainConfig cfg;

        public ChannelSession(MainConfig cfg)
        {
            this.cfg = cfg;

            ExecutionErrors = new ReadOnlyCollection<string>(executionErrors);

            var serverConfigs = cfg.UrTConfig.Servers;
            Servers = serverConfigs.Select(x => new ServerInfo(x)).ToArray();

            constructor = new MessageConstructor(cfg);
        }

        /// <summary>
        /// Infinite loop that keeps gathering and displaying the urt panel
        /// </summary>
        public async Task DisplayContentLoop(DiscordSocketClient client)
        {
            var interval = cfg.UrTConfig.LoopFrequency;

            while (true)
            {
                try
                {
                    await Task.Delay((int)(1000 * interval));

                    executionErrors.Clear(); // Move to a unified execution point when multi session support will be introduced.

                    var errors = await UpdateServers(Servers);
                    var embeds = await GetEmbeds(Servers, errors);
                    await DisplayEmbeds(embeds, client);
                }
                catch (Exception e)
                {
                    Log.Write($"{e.Message}\n{e.StackTrace}", LogCol.Red);
                }
            }
        }

        /// <summary>
        /// Gathers all embeds
        /// </summary>
        async Task<List<EmbedBuilder>> GetEmbeds(ServerInfo[] servers, List<string> occuredErrors)
        {
            List<EmbedBuilder> embedsToDisplay = new();

            if (cfg.DiscordConfig.Localization.EnableTopBanner)
                embedsToDisplay.Add(constructor.CreateTopBanner());

            foreach (var server in servers)
            {
                if (server.Error)
                    continue;

                var panelEmbeds = (await constructor.CreatePanel(server)).GetEmbeds();
                embedsToDisplay.AddRange(panelEmbeds.Select(x => x.GetBuilder()));
            }

            embedsToDisplay.Add(constructor.CreateStatus(string.Join("\n", occuredErrors), cfg));

            return embedsToDisplay;

        }

        /// <summary>
        /// Iterates all servers and updates their state.
        /// Returns a list of occured errors
        /// </summary>
        async Task<List<string>> UpdateServers(ServerInfo[] servers)
        {
            var totalErrors = new List<string>();
            int totalPlayers = 0; // Ready for an upcoming feature: total active players counter

            //Servers
            foreach (var server in servers)
            {
                var errors = UpdateServer(server);
                totalErrors.AddRange(errors);

                totalPlayers += server.Players.Count(x => !x.IsBot);

                await Task.Delay((int)(cfg.UrTConfig.UrTDelay * 1000));
            }

            return totalErrors;
        }

        /// <summary>
        /// Updates the server's state, potentially reports
        /// </summary>
        List<string> UpdateServer(ServerInfo server)
        {
            List<string> errors = new List<string>();

            try
            {
                //connect
                var socket = UrTServerUtil.Connect(server.IP, server.Port);
                if (socket == null)
                {
                    if (server.Important)
                        errors.Add($"Offline: {server.Settings.Name} - {server.ConnectionInfo}");

                    return errors;
                }

                //Get server info
                bool statusSucess = UrTServerUtil.FetchServerInfo(socket, out string status, out string errorMessageInfo);

                //Gather erros for debuging
                if (errorMessageInfo != null)
                    executionErrors.Add(errorMessageInfo);

                // Fetch player info
                string game = "";
                bool rconSuccess = false;
                bool hasRcon = !string.IsNullOrEmpty(server.RconPassword);

                if (!string.IsNullOrEmpty(server.RconPassword) && statusSucess)
                {
                    rconSuccess = UrTServerUtil.FetchPlayerInfo(socket, server.RconPassword, out game, out string errorMessagePlayers);
                    if (errorMessagePlayers != null)
                        executionErrors.Add(errorMessagePlayers);
                }

                // remove invalid players
                server.Players.RemoveAll(x => string.IsNullOrEmpty(x.Nick));

                //server is offline
                if ((!statusSucess && (hasRcon && !rconSuccess)) || (!statusSucess && !hasRcon))
                {
                    if (server.Important)
                        errors.Add($"Offline: {server.Settings.Name} - {server.ConnectionInfo}");

                    return errors;
                }
                //server failed - odd state
                else if (hasRcon && ((statusSucess && !rconSuccess) || (!statusSucess && rconSuccess)))
                {
                    errors.Add($"Failed: {server.Settings.Name} - {server.ConnectionInfo}");
                }
                //parse valid results and populate ServerInfo instance
                else
                {
                    StatusParser.ParseCvars(status, server);
                    StatusParser.ParsePlayers_Status(status, server);

                    if (hasRcon)
                        StatusParser.ParsePlayers_Rcon(game, server, cfg);
                }

                server.FailCount = 0;

                // CONSIDER: keeping the socket open? What is the common behaviour of the UrT server? Will it drop sockets in oreder to protect from DDOS and simmilar attacks?
                //close connection
                socket.Close();
            }
            catch (Exception e)
            {
                var info = $"{server.ConnectionInfo}\n{e.Message}\n{e.StackTrace}";
                Log.Write(info, LogCol.Red);

                server.FailCount++;

                executionErrors.Add(info);

                errors.Add($"Error: {server.Settings.Name} - {server.ConnectionInfo}");
            }

            return errors;
        }

        /// <summary>
        /// Maintains the designated channel and displays the provided embeds.
        /// </summary>
        async Task DisplayEmbeds(List<EmbedBuilder> embeds, DiscordSocketClient client)
        {
            ulong channelID = cfg.DiscordConfig.ServerViewChannelID;
            var channel = (ISocketMessageChannel)client.GetChannel(channelID);

            if (channel == null)
            {
                Log.Write("Channel can't be found", LogCol.Red);
                executionErrors = new List<string>() { $"{channelID} is not a valid channel ID" };
                return;
            }

            var channelMessages = await channel?.GetMessagesAsync(100).FlattenAsync();
            var botsMessages = channelMessages.Reverse().Where(x => x.Author.Id == client.CurrentUser.Id).ToList();

            await Task.Delay(1000);
            int editDelay = (int)(cfg.UrTConfig.EmbedEditDelay * 100);

            foreach (var embed in embeds)
            {
                await Task.Delay(editDelay);

                var msg = botsMessages.FirstOrDefault();
                if (msg != null)
                {
                    await UpdateMessageEmbed((RestUserMessage)msg, embed);
                    botsMessages.Remove(msg);
                }
                else
                    await SendEmbed(channel, embed);
            }

            var loc = cfg.DiscordConfig.Localization;
            foreach (RestUserMessage oldMsg in botsMessages)
            {
                await Task.Delay(250);
                await oldMsg.ModifyAsync(x =>
                {
                    x.Content = loc.BlankEmoji;
                    x.Embed = null;
                });
            }

            Console.WriteLine("Channel edited");
        }

        /// <summary>
        /// Sends a new message with embed
        /// </summary>
        async Task SendEmbed(ISocketMessageChannel channel, EmbedBuilder embed)
        {
            await channel.SendMessageAsync(text: "", embed: embed.Build());
        }

        /// <summary>
        /// Updates existing message by setting the embed
        /// </summary>
        async Task UpdateMessageEmbed(RestUserMessage message, EmbedBuilder embed, string text = "")
        {
            await message.ModifyAsync(x =>
            {
                x.Content = text;
                if (embed != null)
                    x.Embed = embed.Build();
                else
                    x.Embed = null;
            });
        }
    }
}
