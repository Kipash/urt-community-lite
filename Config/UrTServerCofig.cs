﻿using System.Collections.Generic;

namespace UrtPanel.Config
{
    /// <summary>
    /// Server specific settings. For more details refer to the documentation.
    /// </summary>
    public class UrTServerCofig
    {
        public string Name = "Server name";

        public string FooterIconUrl = "";
        public string FooterText = "";

        public bool Important = true;
        public bool ShowBots = true;

        public string RconPassword = "";
        public string IP = "127.0.0.1";
        public int Port = 27960;

        public ServerViewMode DisplayMode = ServerViewMode.Informative;

        public Dictionary<int, string[]> CvarsPerGameType = new();
    }
}
