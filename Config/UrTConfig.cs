﻿namespace UrtPanel.Config
{
    /// <summary>
    /// Urban Terror releated general settings
    /// </summary>
    public class UrTConfig
    {
        public string ThumbnailUrl;
        public float LoopFrequency = 30;
        public float UrTDelay = 0.5f;
        public float EmbedEditDelay = 0.1f;
        public UrTServerCofig[] Servers;
    }
}
