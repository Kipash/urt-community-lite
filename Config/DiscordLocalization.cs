﻿using System.Collections.Generic;

namespace UrtPanel.Config
{
    /// <summary>
    /// Localization and Styling. For more info refer to the documentation.
    /// </summary>
    public class DiscordLocalization
    {
        public bool CensureBotNames = true;
        public string PlayerEmoji = ":man_factory_worker_tone3:";
        public string RedPlayerEmoji = ":person_frowning:";
        public string BluePlayerEmoji = ":police_officer:";
        public string SpectatorEmoji = ":man_detective_tone3:";
        public string BotEmoji = ":robot:";
        public string ScoreEmoji = ":gun:";
        public string BlankEmoji = ":black_small_square:";

        public string NoPlayers = "*no players*";

        public string ClickToJoin = "Click to join: {0}";
        public string StatusMesage = "Last update: {0}";
        public string MissingServers = "Attention! These servers are down!";

        public string Culture = "en-US";
        public string TimeZone = "local";
        public string SmallIconUrl = @"https://www.urbanterror.info/cache/images/texts/support/183.40.png";

        public bool EnableTopBanner = true;
        public string TopBannerIconUrl = "https://www.urbanterror.info/cache/images/texts/support/183.57.png";
        public Dictionary<string, string> TopBannerText = new Dictionary<string, string>() { { "Urban Terror discord viewer", "Made by `BST|Kipash` and `wG*Fuso`, more in !about. Customize this message in the config." } };

        public bool EnableBottomBanner;
        public string BottomBannerIconUrl = "";
        public Dictionary<string, string> BottomBannerText = new Dictionary<string, string>() { { "How to connect with the 'connect link'?", "You have to download the official UrT launcher from here: <https://www.urbanterror.info/downloads/launcher/>" } };

        public Dictionary<int, string> GameTypes = new Dictionary<int, string>()
        {
            { 0, "Free for All" },
            { 1, "Last Man Standing" },
            { 3, "Team Deathmatch" },
            { 4, "Team Survivor" },
            { 5, "Follow the Leader" },
            { 6, "Capture and Hold" },
            { 7, "Capture the Flag" },
            { 8, "Bomb Mode" },
            { 9, "Jump" },
            {10, "Freeze Tag" },
            {11, "Gun Game" },
        };
    }
}
