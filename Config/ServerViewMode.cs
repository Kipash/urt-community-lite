﻿namespace UrtPanel.Config
{
    /// <summary>
    /// Defines different View modes (styles) that the panel can have.
    /// </summary>
    public enum ServerViewMode { none, Informative, Compact }
}
