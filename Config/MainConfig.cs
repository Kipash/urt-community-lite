﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace UrtPanel.Config
{
    /// <summary>
    /// Root of the config. This class is being serialized and deserialzied.
    /// Contains functionality to sanitize the instance.
    /// Saving and Loading as well.
    /// </summary>
    public class MainConfig
    {
        public DiscordConfig DiscordConfig;
        public UrTConfig UrTConfig;
        static MainConfig _instance;

        const string ConfigPath = "appsettings.json";

        static JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            //ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            //DefaultValueHandling = DefaultValueHandling.Populate,
            //NullValueHandling = NullValueHandling.Ignore,
            //MissingMemberHandling = MissingMemberHandling.Ignore,
            ObjectCreationHandling = ObjectCreationHandling.Replace,
        };

        public void SanetizeInstance()
        {
            if (DiscordConfig == null)
                DiscordConfig = new DiscordConfig();
            if (DiscordConfig.CommandPrefix == null)
                DiscordConfig.CommandPrefix = "!";
            if (DiscordConfig.Token == null)
                DiscordConfig.Token = "0";
            if (DiscordConfig.Localization == null)
                DiscordConfig.Localization = new DiscordLocalization();

            if (UrTConfig == null)
                UrTConfig = new UrTConfig();
            if (UrTConfig.ThumbnailUrl == null)
                UrTConfig.ThumbnailUrl = @"https://chrisstman.eu/UCT/thumbnails/";
            if (UrTConfig.Servers == null)
            {
                UrTConfig.Servers = new UrTServerCofig[]
                {
                    new UrTServerCofig()
                    {
                        Name = "Default",
                        FooterIconUrl = @"https://www.urbanterror.info/cache/images/texts/support/183.40.png",
                        Important = false,
                        RconPassword = "",
                        FooterText = "Urban Terror",
                        IP = "127.0.0.1",
                        Port = 27960,
                    }
                };
            }

            foreach (var x in UrTConfig.Servers)
            {
                if (x.CvarsPerGameType != null)
                    continue;

                x.CvarsPerGameType = new Dictionary<int, string[]>()
                {
                    {  0, new string[] { "fraglimit", "timelimit", "g_nextMap" } },
                    {  1, new string[] { "g_maxrounds", "timelimit", "g_nextMap" } },
                    {  3, new string[] { "fraglimit", "timelimit", "g_nextMap" } },
                    {  4, new string[] { "g_maxrounds", "g_roundtime", "timelimit", "g_matchmode", "g_suddendeath", "g_nextMap" } },
                    {  5, new string[] { "timelimit", "g_nextMap" } },
                    {  6, new string[] { "timelimit", "g_nextMap" } },
                    {  7, new string[] { "capturelimit", "timelimit", "g_nextMap", "g_waverespawns", "g_bluewave", "g_redwave" } },
                    {  8, new string[] { "g_maxrounds", "g_roundtime", "timelimit", "g_matchmode", "g_suddendeath", "g_nextMap" } },
                    {  9, new string[] { "timelimit", "g_nextMap" } },
                    { 10, new string[] { "timelimit", "g_nextMap" } },
                    { 11, new string[] { "timelimit", "g_nextMap" } },
                };
            }
        }

        public static MainConfig LoadConfigRaw()
        {
            var json = File.ReadAllText(ConfigPath);
            var cfg = JsonConvert.DeserializeObject<MainConfig>(json, settings);
            return cfg;
        }

        public static string SaveConfigRaw(MainConfig cfg)
        {
            return JsonConvert.SerializeObject(cfg, settings);
        }

        public static MainConfig LoadConfig()
        {
            if (File.Exists(ConfigPath))
            {
                var json = File.ReadAllText(ConfigPath);
                _instance = JsonConvert.DeserializeObject<MainConfig>(json, settings);
                _instance.SanetizeInstance();
            }
            else
            {
                _instance = new MainConfig();
                _instance.SanetizeInstance();

                SaveConfig();

                Log.Write($"Config generated ({ConfigPath}), please fill in Token and other needed configuration!!!", LogCol.Green);
                Environment.Exit(0);
            }
            return _instance;
        }

        public static void SaveConfig()
        {
            var json = JsonConvert.SerializeObject(_instance, settings);
            File.WriteAllText(ConfigPath, json);
        }
    }
}
