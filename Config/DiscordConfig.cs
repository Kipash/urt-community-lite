﻿namespace UrtPanel.Config
{
    /// <summary>
    /// Discord related settings
    /// </summary>
    public class DiscordConfig
    {
        /// <summary>
        /// Discord token
        /// </summary>
        public string Token;

        /// <summary>
        /// Text command prefix that the bot reacts to. Often '!'.
        /// </summary>
        public string CommandPrefix;

        /// <summary>
        /// ID of the channel the bot posts messages
        /// </summary>
        public ulong ServerViewChannelID;

        /// <summary>
        /// Localization & styling
        /// </summary>
        public DiscordLocalization Localization;
    }
}
