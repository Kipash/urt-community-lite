﻿using Discord.Commands;
using System.Threading.Tasks;
using UrtPanel.Panel;
using UrTPanel.Diagnostics;

namespace UrtPanel.Commands
{
    /// <summary>
    /// Specialized commands used for debuging. User needs to have rights to use them.
    /// </summary>
    [Group("Debug")]
    public class DiagnosticCommands : ModuleBase<SocketCommandContext>
    {
        readonly ChannelSession session;

        public DiagnosticCommands(ChannelSession session)
        {
            this.session = session;
        }

        [Command("cfg")]
        [Summary("Displays the bot configuration reducted of sensitive information")]
        [RequireContext(ContextType.DM)]
        [RequireOwnerOrDeveloper]
        public async Task CfgAsync()
        {
            var mes = Diagnostics.ShowConfig();
            foreach (var x in mes.Get())
                await ReplyAsync($"```{x}```");
        }

        [Command("info")]
        [Summary("Displays what the bot is reading of the server")]
        [RequireContext(ContextType.DM)]
        [RequireOwnerOrDeveloper]
        public async Task InfoAsync()
        {
            var mes = Diagnostics.ShowServerInfo(session.Servers);
            foreach (var x in mes.Get())
                await ReplyAsync($"```{x}```");
        }

        [Command("errors")]
        [Summary("returns the current errors for server communication")]
        [RequireContext(ContextType.DM)]
        [RequireOwnerOrDeveloper]
        public async Task GetError()
        {
            if (session.ExecutionErrors.Count != 0)
            {
                var errors = session.ExecutionErrors;
                await ReplyAsync($"Errors:");
                foreach (var x in errors)
                {
                    await ReplyAsync($"{x}");
                }
            }
            else
                await ReplyAsync($"no errors");
        }
    }
}
