﻿using System;
using System.Linq;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;
using UrtPanel.Panel;

namespace UrTPanel.Messages
{
    /// <summary>
    /// Decorator that inserts a CVAR table above the playerlist. 
    /// </summary>
    public class CvarTableDecor : IPanelDecorator
    {
        readonly DiscordLocalization localization;

        public CvarTableDecor(DiscordLocalization localization)
        {
            this.localization = localization;
        }

        public void DecorateView(ServerPanel panel, ServerInfo server)
        {
            server.Cvars.TryGetValue("g_gametype", out string gameTypeString);
            int.TryParse(gameTypeString, out int gameType);

            if (server.Settings.CvarsPerGameType.TryGetValue(gameType, out string[] cvars))
            {
                var tableData = cvars.Select(cvar =>
                {
                    if (server.Cvars.TryGetValue(cvar, out string cvarValue))
                        return new Tuple<string, string>(cvar, cvarValue);
                    else
                        return null;
                })
                .Where(x => x != null)
                .ToArray();

                var table = CreateTable(tableData, server.HasRconInfo ? server.Settings.DisplayMode : ServerViewMode.none);
                foreach (var t in table.Get())
                {
                    var embed = panel.PlayerLists.FirstOrDefault();
                    if (embed == null)
                    {
                        embed = new EmbedData();
                        panel.PlayerLists.Add(embed);
                    }

                    embed.AddFieldTop(localization.BlankEmoji, t.DiscordSanitize(localization));
                }
            }
        }

        const int TableCellWidth_Small = 18;
        const int TableCellWidth_Big = 24;

        DiscordMessage CreateTable(Tuple<string, string>[] data, ServerViewMode mode)
        {
            int width = mode == ServerViewMode.Compact ? TableCellWidth_Big : TableCellWidth_Small;

            var len = (data.Length / 3) + (data.Length % 3 != 0 ? 1 : 0);
            Func<int, Tuple<string, string>> tryGet = (int i) =>
            {
                if (data.Length > i)
                    return data[i];
                else
                    return new Tuple<string, string>("", "");
            };
            Func<string, string> insert = (s) => $"{s.CenterString(width)}";

            DiscordMessage table = new DiscordMessage();
            for (int i = 0; i < len; i++)
            {
                var _1 = tryGet(i + 0);
                var _2 = tryGet(i + 1);
                var _3 = tryGet(i + 2);

                string s = $"`{insert(_1.Item1)}{insert(_2.Item1)}{insert(_3.Item1)}`\n" +
                           $"`{insert(_1.Item2)}{insert(_2.Item2)}{insert(_3.Item2)}`\n";

                table.Add(s);
            }

            return table;
        }
    }
}