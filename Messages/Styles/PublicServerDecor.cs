﻿using System;
using System.Collections.Generic;
using System.Linq;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;
using UrtPanel.Panel;

namespace UrTPanel.Messages
{
    // IMPROVE: create a 3 lane system that jumps between odd numbers to never show a bad wrapping.

    /// <summary>
    /// Decorator for a Public server view - players can't be grouped by a team so it is just a long list ordered by score.
    /// </summary>
    public class PublicServerDecor : IPanelDecorator
    {
        readonly DiscordLocalization localization;

        public PublicServerDecor(DiscordLocalization localization)
        {
            this.localization = localization;
        }

        public void DecorateView(ServerPanel panel, ServerInfo server)
        {
            List<string> playerLines = new();

            int maxPlayerLinesPerEmbed = 10;
            int embedCount = (int)Math.Ceiling((float)server.PlayerCount / maxPlayerLinesPerEmbed);
            int nickWidth = 44;

            foreach (var player in server.Players)
            {
                bool useNick = localization.CensureBotNames && player.IsBot;
                string format = "{0,-" + nickWidth + "}";
                string userName = useNick ? "BOT".CenterString(nickWidth) : string.Format(format, player.Nick);

                string userEmoji = player.IsBot ? localization.BotEmoji : localization.PlayerEmoji;
                playerLines.Add($"{userEmoji} `{userName}`{localization.ScoreEmoji}`{player.Score,3}`\n");
            }

            if (playerLines.Count == 0)
                playerLines.Add(localization.NoPlayers);


            for (int e = 0; e < embedCount; e++)
            {
                var embed = new EmbedData();
                panel.PlayerLists.Add(embed);

                int start = (e * maxPlayerLinesPerEmbed);
                int end = (e + 1) * maxPlayerLinesPerEmbed;
                end = Math.Min(end, playerLines.Count);

                var lines = playerLines.Skip(start).Take(maxPlayerLinesPerEmbed);

                var msg = new DiscordMessage();
                foreach (var x in lines)
                    msg.Add(x);

                var segments = msg.Get();
                for (int i = 0; i < segments.Count; i++)
                {
                    bool firstTitle = (i == 0 && e == 0);
                    var title = firstTitle ? $"`{"Name".CenterString(30)}                 Score  `" : localization.BlankEmoji;
                    embed.AddField(title, segments[i], true);
                }
            }
        }
    }
}