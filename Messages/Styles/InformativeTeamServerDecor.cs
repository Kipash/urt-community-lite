﻿using System.Collections.Generic;
using System.Linq;
using UrtPanel.Config;
using UrtPanel.Messages;
using UrtPanel.Models;
using UrtPanel.Panel;

namespace UrTPanel.Messages
{
    /// <summary>
    /// Decorator for a Team based view - Informative. Goal is to display as many players as possible.
    /// </summary>
    public class InformativeTeamServerDecor : TeamServerDecor, IPanelDecorator
    {
        const int TeamNameWidth = 40;
        readonly DiscordLocalization localization;

        public InformativeTeamServerDecor(DiscordLocalization localization)
        {
            this.localization = localization;
        }

        public void DecorateView(ServerPanel panel, ServerInfo server)
        {
            server.GetCvar("g_namered", out var redName);
            server.GetCvar("g_nameblue", out var blueName);

            server.GetCvar("redScore", out var redScore);
            server.GetCvar("blueScore", out var blueScore);

            var embed = new EmbedData();

            if (server.IsTeamMode)
            {
                WriteTeam(embed, "Blue", blueName, blueScore, server.BlueTeam, server, localization.BluePlayerEmoji);
                WriteTeam(embed, "Red", redName, redScore, server.RedTeam, server, localization.RedPlayerEmoji);
            }
            else
                WriteTeam(embed, "Players", "", "", server.FreeTeam, server, localization.PlayerEmoji);

            if (server.SpecTeam.Any())
                WriteTeam(embed, "Spectator", "", "", server.SpecTeam, server, localization.SpectatorEmoji);

            panel.PlayerLists.Add(embed);
        }

        /// <summary>
        /// Writes whole team's scoretable to the embed
        /// </summary>
        void WriteTeam(EmbedData embed, string defaultTeamName, string teamName, string score, List<PlayerInfo> team, ServerInfo server, string playerEmoji)
        {
            var lines = string.Join("\n", team.Select(pl => PlayerLine(pl, playerEmoji)));
            WriteTeam(embed,
                      $"`{ComposeHeader(defaultTeamName, teamName, score, server).CenterString(TeamNameWidth)}   K   D   A  `",
                      lines);
        }

        /// <summary>
        /// Writes message to a embed
        /// </summary>
        void WriteTeam(EmbedData embed, string title, string toSend)
        {
            if (toSend.Length > 1024)
            {
                var lines = toSend.Split("\n");
                const int MaxCharPerField = 1024;

                string currentSegment = "";
                List<string> segments = new List<string>();
                for (int i = 0; i < lines.Length; i++)
                {
                    var line = lines[i];
                    //if currentSegment would overflow this step => add to buffer | reset buffer
                    if (currentSegment.Length + line.Length > MaxCharPerField)
                    {
                        segments.Add(currentSegment);
                        currentSegment = "";
                    }

                    currentSegment += $"{line}\n";

                    //last index
                    if (i == lines.Length - 1)
                    {
                        segments.Add(currentSegment);
                    }
                }

                for (int i = 0; i < segments.Count; i++)
                {
                    var segment = segments[i];
                    if (i == 0)
                        embed.AddField(title.DiscordSanitize(localization), segment.DiscordSanitize(localization));
                    else
                        embed.AddField(localization.BlankEmoji.DiscordSanitize(localization), segment.DiscordSanitize(localization));
                }
            }
            else
            {
                if (toSend.Length == 0)
                    toSend = localization.NoPlayers;
                embed.AddField(title.DiscordSanitize(localization), toSend.DiscordSanitize(localization));
            }
        }

        /// <summary>
        /// Composes a player line. "{Name} {K} {D} {A}"
        /// </summary>
        string PlayerLine(PlayerInfo pl, string emoji)
        {
            var name = localization.CensureBotNames && pl.IsBot ? $"*`{"Bot".CenterString(35)}`*" : $"`{$"{pl.Nick} ({pl.Auth})".ClampString(35),-35}`";
            var avatar = pl.IsBot ? localization.BotEmoji : emoji;
            return $"{avatar} {name}{localization.BlankEmoji}` {pl.Kills,-3} {pl.Deaths,-3} {pl.Assists,-3}`";
        }

        /// <summary>
        /// Composes a team line. "{Name} ({Score})"
        /// </summary>
        protected string ComposeHeader(string defVal, string name, string score, ServerInfo server)
        {
            var finalName = (!string.IsNullOrEmpty(name) ? name : defVal) + (!string.IsNullOrEmpty(score) ? $" ({score})" : "");
            return finalName;
        }
    }
}