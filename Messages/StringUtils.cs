﻿using UrtPanel.Config;

namespace UrtPanel.Messages
{
    /// <summary>
    /// Common string operations for the Discord and UrT focused bot
    /// </summary>
    public static class StringUtils
    {
        public static string CenterString(this string s, int width)
        {
            if (s.Length % 2 != 0)
                s += " ";

            var pad = (width - s.Length) / 2;
            s = s.PadLeft(pad + s.Length, ' ');
            return s.PadRight(width, ' ');
        }
        public static string ClampString(this string s, int max)
        {
            if (s.Length > max && max > 3)
                s = $"{s.Substring(0, max - 3)}...";

            return s;
        }

        static string[] blacklist = new string[] { "^0", "^1", "^2", "^3", "^4", "^5", "^6", "^7", "^8", "^9", "`", "\"" };
        public static string UrTSanetize(this string s)
        {
            if (s == null)
                return s;

            foreach (var x in blacklist)
            {
                if (x == null)
                    continue;

                s = s.Replace(x, "");
            }

            return s;
        }
        public static string DiscordSanitize(this string s, DiscordLocalization localization)
        {
            if (string.IsNullOrEmpty(s))
                s = localization.BlankEmoji;

            return s;
        }
    }
}